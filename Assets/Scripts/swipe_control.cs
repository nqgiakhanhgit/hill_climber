﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class swipe_control : MonoBehaviour
{
    public Button pre;
    public Button nex;
    public GameObject scrollbar;
    float scrollpos = 0;
    float[] pos;
    int posi = 0;
    // Start is called before the first frame update
    void Start()
    {
        pre.interactable = false;//1
    }
   void Active()
    {
        if (posi == 0)
        {
            pre.interactable = false;
        }
        else
        {
            pre.interactable = true;
        }
        if (posi == pos.Length - 1)
        {
            nex.interactable = false;
        }
        else
        {
            nex.interactable = true;
        }
    }
    public void next()
    {


        if (posi < pos.Length-1)
        {
            posi++;
            scrollpos = pos[posi];
        }
        

    }
    
    public void previos()
    {
        if (posi > 0)
        {
            posi--;
            scrollpos = pos[posi];
        }
      
    }
    // Update is called once per frame
    void Update()
    {
        pos = new float[transform.childCount];
        float distance = 1f / (pos.Length - 1f);
        for (int i = 0; i < pos.Length; i++) {
            pos[i] = distance * i;
        }
        if (Input.GetMouseButton(0))
        {
            scrollpos = scrollbar.GetComponent<Scrollbar>().value;
        }
        else
        {
            for (int i = 0; i < pos.Length; i++)
            {
                if((scrollpos<pos[i]+distance/2)&& (scrollpos > pos[i] - distance / 2))
                {
                    scrollbar.GetComponent<Scrollbar>().value = Mathf.Lerp(scrollbar.GetComponent<Scrollbar>().value, pos[i], 0.15f);
                    posi = i;
                }
            }
        }


        Active();
    }

}
