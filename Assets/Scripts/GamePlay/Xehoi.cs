﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Xehoi : MonoBehaviour
{
    // Start is called before the first frame update
    public WheelJoint2D loptruoc;
    public WheelJoint2D lopsau;
    public float speed;
    private float rot_ilk;
    private float rot_mevcut;
    private float rot_son;
    private float rot_ilkt;
    private float rot_mevcutt;
    private float rot_sont;
    float powerTemp;
    public float v;
    JointMotor2D motore;
    public float motorPower = 1500f,
    brakePower = -14f,
     decelerationSpeed = 0.3f;

    // Car max speed
    public float maxSpeed = 14f;
    void Start()
    {
        rot_mevcut = transform.rotation.eulerAngles.z;
        rot_ilk = rot_mevcut;
        rot_son = rot_mevcut;
        rot_mevcutt = transform.rotation.eulerAngles.z;
        rot_ilkt = rot_mevcutt;
        rot_sont = rot_mevcutt;
    }
    void Flip()//lon Nhao
    {
        rot_mevcut = transform.rotation.eulerAngles.z;
        if (rot_son < rot_mevcut)
        {
            rot_ilk = rot_mevcut;

        }
        else
        if (rot_son > rot_mevcut && rot_son - rot_mevcut > 100)
        {
            rot_ilk = rot_mevcut;
        }
        if (rot_ilk - rot_mevcut > 300)
        {
            Debug.Log("taka");//nhao lon
            rot_ilk = rot_mevcut;
        }
        rot_son = rot_mevcut;
    }
    void TersTakla()//LatNguoc
    {
        rot_mevcutt = transform.rotation.eulerAngles.z;
        if (rot_sont > rot_mevcutt)
        {
            rot_ilkt = rot_mevcutt;

        }
        else
        if (rot_sont < rot_mevcutt && rot_sont - rot_mevcutt > 100)
        {
            rot_ilkt = rot_mevcutt;
        }
        if (rot_mevcutt - rot_ilkt > 300)
        {
            Debug.Log("terstaka");//Lat Nguoc
            rot_ilkt = rot_mevcutt;
        }
        rot_sont = rot_mevcutt;
    }
    //void GetKey()
    //{
    //  v= Input.GetAxis("Horizontal");
    //    if (Input.GetKey(KeyCode.LeftArrow))
    //    {
    //        if (v < 0)
    //        {
    //            v = v * -1;
    //        }
    //    }
    //    if (Input.GetKey(KeyCode.RightArrow))
    //    {
    //        if (v > 0)
    //        {
    //            v = v * -1;
    //        }
    //    }
    //}
    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D orther)
    {
        if (orther.gameObject.CompareTag("Coins"))
        {
            Destroy(orther.gameObject );
        }
        
    }
    void Update()
    {
        v= Input.GetAxis("Horizontal");


        speed = v * Time.deltaTime;

        //if (v > 0)
        //{
        //    for (float i = 0.1f; i < v; i++)
        //    {
        //        speed = i * Time.deltaTime * v;
        //    }
        //}
        //else
        //{
        //    if (v < 0)
        //        for (float i = 0.1f; i < v; i++)
        //        {
        //            speed = -i * v;
        //        }
        //}

        Flip();
        TersTakla();
    }
    private void FixedUpdate()
    {
        
        if (speed == 0f)
        {
            loptruoc.useMotor = false;
            lopsau.useMotor = false;
        }
        else
        {
            loptruoc.useMotor = true;
            lopsau.useMotor = true;

            if (v> 0)
            {
                motore.motorSpeed = Mathf.Lerp(motore.motorSpeed, motorPower, Time.deltaTime * 1.4f);
            }
            else
            {
                if (v < 0)
                {
                    motore.motorSpeed = Mathf.Lerp(motore.motorSpeed, -motorPower, Time.deltaTime * 1.4f);
                }

                else
                {
                    if (speed<-0.2f)
                 
                        motore.motorSpeed = Mathf.Lerp(motore.motorSpeed, 0, Time.deltaTime * 2f);
                    
                    else
                    {
                        motore.motorSpeed = Mathf.Lerp(motore.motorSpeed, 0, Time.deltaTime * decelerationSpeed);
                    }
                }
              
            }
          
            //if (speed > maxSpeed)
            //    motorPower = 0;
            //else
            //    motorPower = powerTemp;

            //// Moving forward
            //if ((Input.GetAxis("Horizontal") > 0))
            //{

            //    // Add force to car back wheel

            //        motore.motorSpeed = Mathf.Lerp(motore.motorSpeed, -motorPower, Time.deltaTime * 1.4f);

            //    // Wheel particles



            //}
            //else
            //{
            //    // Moving backward
            //    if ((Input.GetAxis("Horizontal") < 0))
            //    {
            //        if (speed < -maxSpeed)
            //        {

            //                motore.motorSpeed = Mathf.Lerp(motore.motorSpeed, 0, Time.deltaTime * 3f);
            //        }
            //        else
            //        {

            //                motore.motorSpeed = Mathf.Lerp(motore.motorSpeed, motorPower, Time.deltaTime * 1.4f);
            //        }

            //    }
            //    else
            //    {
            //        // Releasing car throttle and brake

            //            motore.motorSpeed = Mathf.Lerp(motore.motorSpeed, 0, Time.deltaTime * decelerationSpeed);
            //    }
            //}

            motore.maxMotorTorque = 10000;
            loptruoc.motor = motore;
            lopsau.motor = motore;
        }
     



    
    }
  
}
