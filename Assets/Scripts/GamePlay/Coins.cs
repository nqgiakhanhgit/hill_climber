﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    // Start is called before the first frame update
    public int coinsvalue = 1;
    private void OnTriggerEnter2D(Collider2D orther)
    {
        if (orther.gameObject.CompareTag("player"))
        {
            ScroreManager.instance.ChangeScore(coinsvalue);
        }
    }
}
