﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScroreManager : MonoBehaviour
{
    public static ScroreManager instance;
    public TextMeshProUGUI text;
    int score;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
            instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ChangeScore( int coinValue)
    {
        score = score+ coinValue;
        text.text = "X" + score.ToString();
    }
}
