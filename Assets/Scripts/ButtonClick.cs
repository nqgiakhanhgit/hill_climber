﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ButtonClick : MonoBehaviour
{

    public void OnClickButton()
    {
        this.GetComponent<Animation>().Play("ButtonGone");

    }

    public void CompleteScaleButton(string scencename)
    {
        SceneManager.LoadScene(scencename);
        Debug.Log("_________________ Ket thuc scale ___________ Chuyen Scene");
    }
}
